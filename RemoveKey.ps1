##################################################
# CSP キーコンテナ削除
##################################################
<#
.SYNOPSIS
概要
キーコンテナ削除
.DESCRIPTION
秘密鍵を格納したキーコンテナを削除します
.EXAMPLE
RemoveKey.ps1
#>

# キーコンテナ名
$C_ContainerName = "PowerShellEncrypto"


##################################################
# CSP キーコンテナ削除
##################################################
function RSARemoveCSP($ContainerName){
	# アセンブリロード
	Add-Type -AssemblyName System.Security

	# CspParameters オブジェクト作成
	$CSPParam = New-Object System.Security.Cryptography.CspParameters

	# CSP キーコンテナ名
	$CSPParam.KeyContainerName = $ContainerName

	# RSACryptoServiceProviderオブジェクト作成
	$RSA = New-Object System.Security.Cryptography.RSACryptoServiceProvider($CSPParam)

	# CSP キーコンテナ削除
	$RSA.PersistKeyInCsp = $False
	$RSA.Clear()

	# オブジェクト削除
	$RSA.Dispose()

	return
}

#####################################################################
# Main
#####################################################################
$Status = Read-Host -Prompt "Can I remove the key Container? [Y/N]"
if( $Status -eq "Y" ){
	RSARemoveCSP $C_ContainerName
	echo "Remove complete"
}
else{
	echo "Not removed"
}
