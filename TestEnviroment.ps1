﻿<#
.SYNOPSIS
概要
動作環境確認
.DESCRIPTION
スクリプトが実行可能環境であるかを確認します
.EXAMPLE
TestEnviroment.ps1
#>

##########################################################
# 動作可能化の確認
##########################################################
# レジストリ読み取り
function RegGet( $RegPath, $RegKey ){
    # レジストリそのものの有無確認
    if( -not (Test-Path $RegPath )){
        return $null
    }

    # Key有無確認
    $Result = Get-ItemProperty $RegPath -name $RegKey -ErrorAction SilentlyContinue
    # キーがあった時
    if( $Result -ne $null ){
        return $Result.$RegKey
    }
    # キーが無かった時
    else{
        return $null
    }
}

##########################################################
# main
##########################################################

# PowerShell バージョン確認
$Vertion = $PSVersionTable
if( $Vertion.PSVersion.Major -ge 3 ){
	echo "This environment is good."
}
else{
	# .NET Framework バージョン確認
	$RootReg = "HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP"
	$Datas = dir $RootReg

	foreach( $Data in $Datas ){
		$Terget = $Data.Name
		$Terget = $Terget -replace "HKEY_LOCAL_MACHINE", "HKLM:"
		if( (Split-Path $terget -Leaf) -eq "v4" ){
			# .NET Framework 4 以降が入っている
			echo ".NET Framework vertion is ok. Please install PowerShell 3.0 or above."
			echo ""
			echo "Download Windows Management Framework 5.0(PowerShell 5.0)"
			echo "https://www.microsoft.com/en-us/download/details.aspx?id=50395"
			exit
		}
	}

	# .NET Framework 4 以降が入っていない
	echo ".NET Framework and PowerShell vertion was NG. Please install .NET Framework 4.5.2 and PowerShell 3.0 or above."
	echo ""
	echo "Microsoft .NET Framework 4.6.1 (Online Installer)"
	echo "https://www.microsoft.com/ja-jp/download/details.aspx?id=49982"
	echo ""
	echo "Download Windows Management Framework 5.0(PowerShell 5.0)"
	echo "https://www.microsoft.com/en-us/download/details.aspx?id=50395"
}


